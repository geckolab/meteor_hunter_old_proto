﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsMove : MonoBehaviour
{
    List<Transform> lights;
    Vector3 initialTopLightPos;
    // Start is called before the first frame update
    void Start()
    {
        lights = new List<Transform>();
        foreach(Transform tr in transform)
        {
            lights.Add(tr);
        }
        initialTopLightPos = lights[0].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Transform tr in transform)
        {
            tr.Translate(new Vector3(0, -0.01f, 0),Space.World);
            if (tr.position.y < -2)
                tr.transform.position = initialTopLightPos;
        }
    }
}
