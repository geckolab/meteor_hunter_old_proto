﻿using UnityEngine;

public class Deflector : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D collision) // when trying to leave the game area circle
    {
        Vector3 meteorExitPos = collision.gameObject.transform.position;
        collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Vector2 forceToAdd = -meteorExitPos.normalized;

        bool left = (Random.Range(0, 2) == 0);
        float randomAngle = Random.Range(0, 20f);
        if (!left)
            randomAngle *= -1;

        Vector2 forceRotated = Rotate(forceToAdd, randomAngle);
        collision.gameObject.GetComponent<Rigidbody2D>().velocity = (forceRotated.normalized); // adding force = exit position, causing it to center. Should randomize instead?

    }

    private static Vector2 Rotate(Vector2 v, float degrees) // TODO: move to some math helper or sth
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }
}
