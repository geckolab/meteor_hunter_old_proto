﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    Vector2 beg;
    Vector2 end;
    public GameObject laser;

    TouchPhase prevPhase;

    void passInputToLaser(Vector3 inputPos, bool isTouch)
    {
        if (isTouch)
        {
            Vector3 v = Camera.main.ScreenToWorldPoint(inputPos);
            v.z = 0;
            laser.transform.position = v;
            laser.SetActive(true);
        }
        else
        {
            laser.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        // PC EMULATION
#if UNITY_EDITOR
        passInputToLaser(Input.mousePosition, Input.GetMouseButton(0));
#endif

        //Mobile device:
#if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0)
        {
            Touch t = Input.touches[0];
            Debug.Log("input!:" + t.phase + ", " + t.deltaPosition);

            passInputToLaser(t.position, t.phase == TouchPhase.Moved);
        }
#endif
    }
}
