﻿using TMPro;
using UnityEngine;

public class TouchDetect : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    public TextMeshProUGUI myTMP;
    public static int val = 0;

    private void OnTriggerEnter2D(Collider2D collision) // on detect touch
    {
        if (gameObject.activeSelf)
        {
            collision.GetComponent<ITouchable>().ReactOnTouch();
            Debug.LogError("JEB!");
            val++;
            myTMP.text = "" + val;
        }
    }

}
