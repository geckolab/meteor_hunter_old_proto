﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallMeteorScript : MonoBehaviour, ITouchable
{
    bool reactionEnabled;

    int touches = 0;

    const int TOUCHES_TO_REMOVE = 5;

    public void ReactOnTouch()
    {
        if (reactionEnabled)
        {
            touches++;
            if (touches == TOUCHES_TO_REMOVE)
            {
                Vector3 escapeVector = Random.insideUnitCircle.normalized;
                gameObject.transform.Translate(15 * escapeVector);
                touches = 0;
            }

        }
    }

    public void EnableReaction() // called when big meteor falls apart.
    {
        GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<Rigidbody2D>().WakeUp();
        GetComponent<Rigidbody2D>().velocity = Random.insideUnitCircle * 2;

        StartCoroutine(DelayedDetach());
        StartCoroutine(DelayedEnable());
    }

    IEnumerator DelayedDetach()
    {
        yield return new WaitForSeconds(0.1f);

        this.transform.parent = null;

    }
    IEnumerator DelayedEnable()
    {
        yield return new WaitForSeconds(0.5f);
        reactionEnabled = true;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

}
