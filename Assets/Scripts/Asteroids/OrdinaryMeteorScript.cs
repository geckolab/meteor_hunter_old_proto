﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrdinaryMeteorScript : MonoBehaviour, ITouchable

{
    public void ReactOnTouch()
    {
        // TODO: move it somewhere else
        Vector3 escapeVector = Random.insideUnitCircle.normalized;
        gameObject.transform.Translate(15 * escapeVector);

        gameObject.GetComponent<Rigidbody2D>().velocity = (-Random.insideUnitCircle.normalized);
    }

    // Start is called before the first frame update
    void Start()
    {

    }


}
