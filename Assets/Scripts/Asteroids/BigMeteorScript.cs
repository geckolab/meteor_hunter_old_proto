﻿using System;
using UnityEngine;

public class BigMeteorScript : MonoBehaviour, ITouchable
{
    int touches = 0;

    private void Awake()
    {
        touches = 0;
    }

    public void ReactOnTouch()
    {
        touches++;
        if (touches == 15)
        {
            FallApart();
        }
    }


    private void FallApart()
    {
        GetComponent<CircleCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().simulated = false;
        int childcount = transform.childCount;
        for (int i = 0; i < childcount; i++)
        {
            try
            {
                transform.GetChild(i).GetComponent<SmallMeteorScript>().EnableReaction();
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }
        }
    }

}
