﻿using UnityEngine;

public class Asteroids : MonoBehaviour // TODO: rename
{

    public CircleCollider2D gameArea;

    // Start is called before the first frame update
    void Start()
    {
        if (gameArea == null)
            Debug.Log("game area not attached to the asteroid spawning script!");

        foreach (Transform t in transform) // initial random velocity
        {
            if ((t.position - gameArea.transform.position).magnitude > gameArea.radius)
            {
                Debug.Log("an asteroid outside the game area!");
                t.position = Random.insideUnitCircle * Random.Range(gameArea.radius * 0.5f, gameArea.radius);
            }

            Vector2 velocity = Random.insideUnitCircle;
            Debug.Log("adding force: start");

            t.gameObject.GetComponent<Rigidbody2D>().velocity = (velocity);
        }

    }

}
