﻿internal interface ITouchable
{
    void ReactOnTouch();
}