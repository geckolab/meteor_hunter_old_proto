﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxAnimator : MonoBehaviour //TODO: move to some other folder/namespce
{

    Skybox skybox;
    float rotation = 0;

    // Start is called before the first frame update
    void Start()
    {
        skybox = GetComponent<Skybox>();
    }

   
    void Update()
    {
        if (rotation < 360)
            rotation += 0.02f;
        else
            rotation = 0;
        skybox.material.SetFloat("_Rotation", rotation);
    }

}
